alph = [5,200,15000];
bi = [0.0027, 1.08, 81.08];
for i=1:3
    
    filename='TemperatureCoord/Temperature_x1'+ string(i);
    D = load(filename+'.txt');
    t=figure;

    x = D(:,2);
    y1 = D(:,1);
    plot(x,y1,'Color', "#3eb489", 'LineWidth',2);
    hold on;
    
    
    filename='TemperatureCoord/Temperature_x2'+string(i);
    F = load(filename+'.txt');
    y2 = F(:,1);
    plot(x,y2,'Color', "#7B68EE", 'LineWidth',2);
    
    
    filename='TemperatureCoord/Temperature_x3'+string(i);
    H = load(filename+'.txt');
    y3 = H(:,1);
    plot(x,y3,'Color', "#F08080", 'LineWidth',2);
    hold off;
    grid on;
    
    xlabel('x, м', 'fontsize', 12);
    ylabel('T, ℃', 'fontsize', 12);
    legend('Fo = 0.1FoMax','Fo = 0.5FoMax','Fo = 0.9FoMax', 'fontsize',10);
    png_name='Graphics/Tcoord'+string(alph(i));
    saveas(t, png_name + '.png');
    close
end