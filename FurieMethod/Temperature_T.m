filename='TemperatureTime/Temperature_time';
bi = [0.0027, 1.08, 81.08];
for i=1:2:3
    full_file1=filename + strcat(string(i), '1')+'.txt';
    D = load(full_file1);
    t=figure;
    x = D(:,2);
    y1 = D(:,1);
    plot(x,y1,'Color', "#3eb489", 'LineWidth',2);
    hold on;
    
    full_file2=filename + strcat(string(i), '2')+'.txt';
    E = load(full_file2);
    y2 = E(:,1);
    plot(x,y2,'Color', "#7B68EE", 'LineWidth',2)

    full_file3=filename + strcat(string(i), '3')+'.txt';
    F = load(full_file3);
    y3 = F(:,1);
    plot(x,y3,'Color', "#F08080", 'LineWidth',2)
    hold off;
    grid on;

    xlabel('t, c', 'fontsize', 12);
    ylabel('T, ℃', 'fontsize', 12);
    legend('x = 0','x = δ/4','x=δ/2', 'fontsize',10);
    png_name='Graphics/Tt_δ_bi-'+ string(bi(i))+'.png';
    saveas(t, png_name);
    close
end 