module Furie
contains

    ! указать число узлов отчет 2
	function theta(Bi,Fo,eps,X,ind)
        implicit none
        real(8) :: eps, Bi,theta_n, theta, Fo, eps0, mu_n, A_n, X
        integer :: n, ind
        eps0=1e-6
        theta=0
        n=0
        do
            n=n+1
            mu_n = bisection(eps0, n, Bi)
            A_n = 2*sin(mu_n)/(mu_n+sin(mu_n)*cos(mu_n))
            theta_n = A_n*exp(-Fo*mu_n**2)*cos(mu_n*X)
            theta = theta + theta_n
            if (abs(theta_n)<eps) exit  
        end do
        if (ind==1) then 
            write(*,'(A6, f6.2, A14, i1)') '  Fo = ', Fo, ' iterations = ', n
        end if
    end function


    function bisection(eps,n,Bi)
        implicit none
        integer :: n
        real(8) :: a,b,fb,c,eps,fc,delta,Bi,bisection
        a = acos(-1.0)*(n-1)
        b = acos(-1.0)*(n-0.5)
        fb = 1/tan(b) - b/Bi
        do
            delta=abs(a-b)
            c = (a+b)/2
            fc = 1/tan(c) - c/Bi
            if(fc*fb>0) then
                b=c
                fb = fc
            else
                a=c
            endif 
            if (delta<=eps) exit 
        end do
        bisection = c
    end function


    function FoMax(Bi)
        implicit none
        real(8) :: FoMax, Bi
        if (Bi < 1.25) then
            FoMax = 3.11/Bi**0.88
        else if (Bi <= 20 .and. Bi >= 1.25) then
            FoMax = 2.76/Bi**0.31
        else
            FoMax = 1.10
        end if
    end function


    character(len=20) function str(k)
        implicit none
		integer :: k
		write (str, *) k
		str = adjustl(str)
    end function

end module


program FurieAlg
    use Furie
    implicit none
    real(8) :: delta,lmbd,l,t_ext,t_int,eps,w,X,h,x_g,dF,Fo_grid, & 
    a,tau,Bi,Fo_m,Fo,theta_res,alph,temp,time, density,c, start, finish
    real(8), dimension(3) :: alpha, x_cut
    integer :: i, j, n=100, k, t, v=100, ios=0
    
    ! Test
    l = 0
    delta = 8e-3
    a = 5.71
    lmbd=0.74
    t_int = 80
    t_ext=15
    X = 2*l/delta
    eps=1e-7
    alpha=[5, 200, 15000]
    x_cut=[real(8) :: 0, delta/4, delta/2]
    density=2500
    c=0.67*10**3
    a=lmbd/(density*c)
    Fo = 6 !4*(a*tau)/(delta**2)
    Bi = 0.45
    X=0
    theta_res = theta(Bi,Fo,eps,X,0)
    write(*,'(a5,f4.2,/a5,f3.1)') 'Bi = ',Bi, 'Fo = ',Fo
    write(*,'(/,A14,f6.3,A10,/,A7,f5.3)') 'Temperature = ',t_ext+theta_res*(t_int-t_ext), ' degrees C', &
    'Teta = ' , theta_res
    ! End test

    ! Graphic 1 T = T(x)
    h = delta/(n-1)
    Fo = 0
    call cpu_time(start)
    do k=1,3
        w = (0.1+0.4*(k-1))
        do i=1,size(alpha)
            open(i, file='TemperatureCoord/Temperature_x'//trim(str(k))//trim(str(i))//'.txt', iostat=ios)
            if (ios /= 0) then 
                call system('md TemperatureCoord')
                open(i, file='TemperatureCoord/Temperature_x'//trim(str(k))//trim(str(i))//'.txt')
            end if
            Bi = (alpha(i)*delta)/(2*lmbd)
            Fo_m = FoMax(Bi)
            Fo_grid = w*Fo_m
            do j=1,n
                x_g = (l + (j-1)*h)/2
                X = 2*x_g/delta
                theta_res = theta(Bi,Fo_grid,eps,X,0)
                temp = t_ext+theta_res*(t_int-t_ext)
                write(i,*) temp, x_g
            end do
        close(i)
        end do

    ! Graphic 2 T = T(t)
        if (k==1 .or. k==3) then
            Bi = (alpha(k)*delta)/(2*lmbd)
            Fo_m = FoMax(Bi)
            dF = Fo_m/v
            do j=1,size(x_cut)
                open(i, file='TemperatureTime/Temperature_time'//trim(str(k))//trim(str(j))//'.txt', iostat=ios)
                if (ios /= 0) then 
                    call system('md TemperatureTime')
                    open(i, file='TemperatureTime/Temperature_time'//trim(str(k))//trim(str(j))//'.txt')
                end if
                X=2*x_cut(j)/delta
                do while(Fo<Fo_m)
                    theta_res=theta(Bi,Fo,eps,X,0)
                    temp = t_ext+theta_res*(t_int-t_ext)
                    time = (Fo*delta**2)/(4*a)
                    write(i,*) temp, time
                    Fo=Fo+dF
                end do
                Fo=0
                close(k)
            end do
        end if
    end do
    call cpu_time(finish)
    write(*,'(/,f5.2,a32,/)') finish, 'sec is required for writing data'
    ! 2)
    eps=1e-10
    write(*,'(2/, A6, es7.1)') 'eps = ', eps
    do i=1,3
        Bi = (alpha(i)*delta)/(2*lmbd)
        write(*,'(/,A4, f7.3)') 'Bi = ', Bi
        do j=1,3
            Fo_m = FoMax(Bi)
            Fo = Fo_m*(0.1+0.4*(j-1))
            theta_res = theta(Bi,Fo,eps,X,1)
        end do
        write(*,*)
    end do
    write(*,*)

    ! 3)
    Fo=0
    X=0
    eps = 0.1
    write(*,'(A6, f4.2,/)') 'eps = ', eps
    do i=1,3,2
        Bi = (alpha(i)*delta)/(2*lmbd)
        Fo_m = FoMax(Bi)
        dF = Fo_m/1000
        do while(Fo < Fo_m)
            Fo = Fo + dF
            theta_res = theta(Bi,Fo,eps,X,0)
        end do
        write(*,'(A4, f7.3, /, A6, f7.3, A5, f8.3, A1, /)') 'Bi = ', Bi, '  Fo = ', Fo, ' --> ', (Fo*delta**2)/(4*a), 's'
        Fo = 0
    end do
end program
