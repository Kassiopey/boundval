module IFinite

    contains
    character(len=20) function str(k)
        implicit none
		integer :: k
		write (str, *) k
		str = adjustl(str)
    end function


    function FoMax(Bi)
        implicit none
        real(8) :: FoMax, Bi
        if (Bi < 1.25) then
            FoMax = 3.11/Bi**0.88
        else if (Bi <= 20 .and. Bi >= 1.25) then
            FoMax = 2.76/Bi**0.31
        else
            FoMax = 1.10
        end if
    end function


    function column(x, knots, delta) result(k)
        implicit none
        real(8) :: x, delta
        integer :: k, knots
        k = (2*x/delta)*(knots-1) + 1
    end function


    function tape_solver(im, a, b, c, d)
        implicit none
        integer, intent(in) :: im
        integer :: i
        real(8), dimension(im), intent(in) ::  a, b, c, d
        real(8), dimension(im) ::  y, tape_solver
        real(8) :: alpha(im), beta(im), gamma
        alpha(1) = -a(1)/b(1)
        beta(1) = -d(1)/b(1)
        do i=2,im-1
            gamma = (b(i)+c(i)*alpha(i-1))
            alpha(i) = -a(i)/gamma
            beta(i) = -(d(i)+c(i)*beta(i-1))/gamma
        end do
        y(im) = -(d(im)+c(im)*beta(im-1))/(b(im)+c(im)*alpha(im-1))
        do i=im-1,1,-1
            y(i) = alpha(i)*y(i+1) + beta(i)
        end do
        tape_solver = y
    end function tape_solver


    function ExplTeta(im, Bi, Fo, dFo) result(tetaM)
        implicit none
        integer :: t, n, i, im
        real(8) :: dX, dFo, Bi, Fo
        real(8), dimension(im) :: a, b, c, d, tetaM
        dX = 1.0/(im-1)
        n = Fo/dFo + 1
        a = [real(8) :: 2.0/(dX**2), (1/(dX**2), i=2,im-1), 0]
        b = [(-1.0/dFo-a(1), i=1,im-1)]
        b(im) = b(1)-2*Bi/dX
        c = [real(8) :: 0, (a(i), i=2,im-1), a(1)]
        tetaM = 1 
        do t=1,n-1
            d = tetaM/dFo
            tetaM = tape_solver(im,a,b,c,d)
        end do
    end function
    

    function tetaFinite(Bi, Fo, im) result(teta1)
        real(8) :: dX, dFo, Bi, Fo
        integer :: n, im, j
        real(8), dimension(im) :: teta1
        dX = 1.0/(im-1)
        dFo = (dX**2)/2
        n = Fo/dFo + 1
        teta1(:) = 1
        do j=1,n-1
            teta1 = finiteDiff(teta1, Bi, im)
        end do
    end function tetaFinite


    function finiteDiff(teta1, Bi, im)
        implicit none
        real(8) :: dX, dFo, Bi, r
        integer :: i, im
        real(8), dimension(im) :: teta1, finiteDiff
        dX = 1.0/(im-1)
        dFo = (dX**2)/2
        r = dFo/dX**2
        do i=2,im-1
            finiteDiff(i)=r*(teta1(i+1)+teta1(i-1)) + teta1(i)*(1-2*r)
        end do
        finiteDiff(1) = finiteDiff(2)
        finiteDiff(im) = finiteDiff(im-1)/(1+Bi*dX)
    end function


    function FurieTeta(Bi,Fo,eps,X)
        implicit none
        real(8) :: eps, Bi, teta_n, FurieTeta, Fo, eps0, mu_n, A_n, X
        integer :: n
        eps0=1e-6
        FurieTeta=0
        n=0
        do
            n=n+1
            mu_n = bisection(eps0, n, Bi)
            A_n = 2*sin(mu_n)/(mu_n+sin(mu_n)*cos(mu_n))
            teta_n = A_n*exp(-Fo*mu_n**2)*cos(mu_n*X)
            FurieTeta = FurieTeta + teta_n
            if (abs(teta_n)<eps) exit  
        end do
    end function


    function bisection(eps,n,Bi) result(c)
        implicit none
        integer :: n
        real(8) :: a,b,fb,c,eps,fc,delta,Bi
        a = acos(-1.0)*(n-1)
        b = acos(-1.0)*(n-0.5)
        fb = 1/tan(b) - b/Bi
        do
            delta=abs(a-b)
            c = (a+b)/2
            fc = 1/tan(c) - c/Bi
            if(fc*fb>0) then
                b=c
                fb = fc
            else
                a=c
            endif 
            if (delta<=eps) exit 
        end do
    end function


end module


program ImpFinite
    use IFinite

    implicit none
    integer :: im, n1, n2, i, j, knots(4), alphaArr(3),dfArr(4), q, v, alpha, s, k, p, n
    real(8) :: Bi, Fo, dX, dFo1, dFo2, tetaF, X, eps=1e-7, delta1, delta2, delta3, x_split(3), &
    hc, density, delta, t_int, t_ext, lmbd, dFo, Fo_m, w, time, dcoord, ac
    real(8), allocatable :: tetaImpl(:), tetaExpl(:), a(:), b(:), c(:), d(:)
    knots = [11, 21, 41, 81]
    alphaArr=[5, 200, 15000]
    dfArr = [1,5,20,100]
    Bi = 0.45
    Fo = 5
    t_int = 80      
    t_ext=15
    delta = 8e-3
    lmbd=0.74
    density=2500
    hc=670
    ac=lmbd/(density*hc)
    x_split = [real(8) :: 0, delta/4, delta/2]
    X = 0
    im = 101
    dX = 1.0/(im-1)
    dFo2 = n*(dX**2)/2
    n2 = Fo/dFo2 + 1
    allocate(tetaExpl(im))
    tetaF = FurieTeta(Bi, Fo, eps, X)
    tetaExpl = ExplTeta(im, Bi, Fo, dFo2)
    write(*,'(/, a9)') 'Testing :'
    write(*,'(a11, f7.5)') 'ExFinite - ', tetaExpl(1), &
                'Furie - ',tetaF
    deallocate(tetaExpl)

    im = knots(2)
    dX = 1.0/(im-1)
    dFo1 = (dX**2)/2
    allocate(tetaImpl(im))
    allocate(tetaExpl(im))
    tetaImpl = tetaFinite(Bi, Fo, im)
    delta1 = abs(tetaImpl(1)-tetaF)
    do j=1,4
        n = dfArr(j)
        write(*,'(/, a48, /, i3, a4)') '################################################',n,'*dFo'
        dFo2 = n*(dX**2)/2
        tetaExpl = ExplTeta(im, Bi, Fo, dFo2)
        delta2 = abs(tetaExpl(1)-tetaF)
        delta3 = abs(tetaImpl(1)-tetaExpl(1))
        write(*,'(f10.8)') dFo2
        write(*,'(a48, /, a8, i2, /)') '------------------------------------------------' , &
            'knots = ', im
        write(*,'(a11, f7.5)') 'ImFinite - ', tetaImpl(1), &
            'ExFinite - ', tetaExpl(1), &
            'Furie - ',tetaF
        write(*,'(/,a26, f10.8,/,a25, f10.8,/,a28, f10.8, /)') 'Delta(Furie, ImpFinite) = ', delta1, &
            'Delta(Furie, ExFinite) = ', delta2, &
            'Delta(ImFinite, ExFinite) = ', delta3
        write(*,'(a33, f4.2, a1, /, a32, f4.2, a1, /)') 'Relative Err(Furie, ImpFinite) = ', 100*delta1/tetaF,'%', &
            'Relative Err(Furie, ExFinite) = ', 100*delta2/tetaF,'%'
        write(*,'(a36, f5.2, a1, /, a35, f5.2, a1, /)') 'Relative Err(ExFinite, ImpFinite) = ', 100*delta3/tetaExpl(1),'%', &
            'Relative Err(ImFinite, ExFinite) = ', 100*delta3/tetaImpl(1),'%'
    end do
    write(*,'(a48)') '################################################'
    deallocate(tetaExpl)
    deallocate(tetaImpl)

    ! do j=1,4
    !     n = dfArr(j)
    !     open(j, file='ErrorFinite/ErrorKnots'//trim(str(n))//'dFo.txt')
    !     do v=11,100
    !         dX = 1.0/(v-1)
    !         dFo1 = (dX**2)/2
    !         dFo2 = n*(dX**2)/2
    !         allocate(tetaExpl(v))
    !         allocate(tetaImpl(v))
    !         tetaImpl = tetaFinite(Bi, Fo, v)
    !         tetaExpl = ExplTeta(v, Bi, Fo, dFo2)
    !         delta1 = abs(tetaImpl(1)-tetaF)
    !         delta2 = abs(tetaExpl(1)-tetaF)
    !         write(j,*) v, delta2, delta1
    !         deallocate(tetaExpl)
    !         deallocate(tetaImpl)
    !     end do
    !     close(j)
    ! end do
    
    im = 81
    n=5
    allocate(a(im))
    allocate(b(im))
    allocate(c(im))
    allocate(d(im))
    dX = 1.0/(im-1)
    dFo = n*(dX**2)/2
    a = [real(8) :: 2.0/(dX**2), (1/(dX**2), i=2,im-1), 0]
    b = [(-1.0/dFo-a(1), i=1,im-1)]
    c = [real(8) :: 0, (a(i), i=2,im-1), a(1)]
    Fo = 0
    dcoord = delta/(2*(im-1))
    do j=1,3
        alpha = alphaArr(j)
        Bi = (alpha*delta)/(2*lmbd)
        Fo_m = FoMax(Bi)
        b(im) = b(1)-2*Bi/dX
        n2 = Fo_m/dFo + 1
        allocate(tetaExpl(im))
        do q = 1,3
            open(q, file='TempX/TempX'//trim(str(alpha))//'_'//trim(str(q))//'.txt')
            w = 0.1+0.4*(q-1)
            tetaExpl = ExplTeta(im, Bi, w*Fo_m, dFo)
            write(q,*) tetaExpl(:)*(t_int - t_ext) + t_ext
            write(q,*) (dcoord*(p-1), p=1, im)
            close(q)
        end do

        if (j==1 .or. j==3) then
            do v=1,3
                open(j, file='TempTime/TempTime'//trim(str(alpha))//'_'//trim(str(v))//'.txt')
                tetaExpl = 1
                k = column(x_split(v), im, delta)
                write(j,*) (Fo*delta**2)/(4*ac), tetaExpl(k)*(t_int - t_ext) + t_ext
                do i=1,n2-1
                    Fo = Fo + dFo
                    time = (Fo*delta**2)/(4*ac)
                    d = tetaExpl/dFo
                    tetaExpl = tape_solver(im, a, b, c, d)
                    write(j,*) time, tetaExpl(k)*(t_int - t_ext) + t_ext
                end do
                Fo = 0
                close(j)
            end do
        end if
        deallocate(tetaExpl)
    end do
    deallocate(a)
    deallocate(b)
    deallocate(c)
    deallocate(d)

end program
