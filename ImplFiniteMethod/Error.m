n=[1,5,20,100];
for i=1:4
    c = load('ErrorFinite/ErrorKnots'+string(n(i))+'dFo.txt');
    x=c(:,1);
    y1=c(:,2);
    y2=c(:,3);
    a=figure;
    plot(x,log(y1),'Color', "#3eb489", 'LineWidth',2);
    hold on;
    plot(x,log(y2),'Color', "#7B68EE", 'LineWidth',2);
    xlabel('Im', 'fontsize', 13);
    ylabel('log10(|Furie-Finite|)', 'fontsize', 13);
    legend('Explicit'+string(n(i))+'dFo','Implicit', 'fontsize',10);
    grid on;
    png_name='Graphics/Error'+string(n(i))+'.png';
    saveas(a, png_name);
    close
end