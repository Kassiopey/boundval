module FiniteM
    contains
    

    character(len=20) function str(k)
        implicit none
		integer :: k
		write (str, *) k
		str = adjustl(str)
    end function


    function string(Fo, knots) result(m)
        implicit none
        real(8) :: Fo, dX
        integer :: m, knots
        dX = 1.0/(knots-1)
        m = 2*Fo/(dX**2) + 1
    end function


    function column(x, knots, delta) result(k)
        implicit none
        real(8) :: x, delta
        integer :: k, knots
        k = (2*x/delta)*(knots-1) + 1
    end function
    

    function FoMax(Bi)
        implicit none
        real(8) :: FoMax, Bi
        if (Bi < 1.25) then
            FoMax = 3.11/Bi**0.88
        else if (Bi <= 20 .and. Bi >= 1.25) then
            FoMax = 2.76/Bi**0.31
        else
            FoMax = 1.10
        end if
    end function


    function finiteDiff(Bi, Fo_m, dFo, im) result(theta)
        implicit none
        real(8), allocatable :: theta(:,:)
        real(8) :: dX, dFo, Bi, Fo_m, r
        integer :: i, j, n, im
        dX = 1.0/(im-1)
        n = Fo_m/dFo + 1
        allocate(theta(n, im))
        theta(1,:) = 1
        r = (dFo/dX**2)
        do j=2,n
            do i=2,im-1
                theta(j,i)=r*(theta(j-1,i+1)+theta(j-1,i-1)) & 
                + theta(j-1,i)*(1-2*r)
            end do
            theta(j,1) = theta(j,2)
            theta(j,im) = theta(j,im-1)/(1+Bi*dX)
        end do
    end function


    function theta(Bi,Fo,eps,X)
        implicit none
        real(8) :: eps, Bi,theta_n, theta, Fo, eps0, mu_n, A_n, X
        integer :: n
        eps0=1e-6
        theta=0
        n=0
        do
            n=n+1
            mu_n = bisection(eps0, n, Bi)
            A_n = 2*sin(mu_n)/(mu_n+sin(mu_n)*cos(mu_n))
            theta_n = A_n*exp(-Fo*mu_n**2)*cos(mu_n*X)
            theta = theta + theta_n
            if (abs(theta_n)<eps) exit  
        end do
    end function


    function bisection(eps,n,Bi) result(c)
        implicit none
        integer :: n
        real(8) :: a,b,fb,c,eps,fc,delta,Bi
        a = acos(-1.0)*(n-1)
        b = acos(-1.0)*(n-0.5)
        fb = 1/tan(b) - b/Bi
        do
            delta=abs(a-b)
            c = (a+b)/2
            fc = 1/tan(c) - c/Bi
            if(fc*fb>0) then
                b=c
                fb = fc
            else
                a=c
            endif 
            if (delta<=eps) exit 
        end do
    end function

end module


program Calc
    use FiniteM
    implicit none        
    real(8) :: Bi, delta, lmbd, Fo_m, c, density, t_ext, t_int, a, dX, error, Fo ,&
    dFo, time, X, w, x_split(3), coord=0, dcoord, theta_res, eps=1e-3, start, finish
    integer :: alphaArr(3), Im(4), n, knots, alpha, & 
    i, j, k, s, v, q, p
    real(8), allocatable :: thetaMatrix(:,:)
    t_int = 80      
    t_ext=15
    delta = 8e-3
    lmbd=0.74
    density=2500
    c=670
    x_split = [real(8) :: 0, delta/4, delta/2]
    alphaArr = [5, 200, 15000]
    Im = [11, 21, 41, 81]
    knots = 81
    a=lmbd/(density*c)
    dX = 1.0/(knots-1)
    dFo = (dX**2)/2
    Bi = 0.45
    Fo = 6
    n = Fo/dFo + 1
    print*,n
    X=0
    allocate(thetaMatrix(n,knots))
    theta_res = theta(Bi,Fo,eps,X)
    thetaMatrix = finiteDiff(Bi, Fo, dFo, knots)
    write(*,'(/,a19,f7.5,/a18,f7.5,2/)') 'FiniteAlg result - ', thetaMatrix(n,1), 'FurieAlg result - ',theta_res
    deallocate(thetaMatrix)
    !Test
    do i=1,4
        knots = Im(i)
        dX = 1.0/(knots-1)
        dFo = (dX**2)/2
        n = Fo/dFo + 1
        allocate(thetaMatrix(n,knots))
        thetaMatrix = finiteDiff(Bi, Fo, dFo, knots)
        error = abs(thetaMatrix(n,1)-theta_res)
        write(*,'(a5, i2)') 'Im = ', knots
        write(*,'(/,a19,f7.5,/a18,f7.5)') 'FiniteAlg result - ', thetaMatrix(n,1), 'FurieAlg result - ',theta_res
        write(*,'(a8,f7.5)') 'Error = ', error
        write(*,'(a17, f8.5, a1, /)') 'Relative error = ', 100*error/theta_res, '%'
        deallocate(thetaMatrix)
    end do
    !End test

    knots = Im(4)
    dX = 1.0/(knots-1)
    dFo = (dX**2)/2
    dcoord = delta/(2*(knots-1))

    !Graphics data
    Fo = 0
    call cpu_time(start)
    do j=1,3
        alpha = alphaArr(j)
        Bi = (alpha*delta)/(2*lmbd)
        Fo_m = FoMax(Bi)
        n = Fo_m/dFo + 1
        allocate(thetaMatrix(n,knots))
        print*,n
        thetaMatrix = finiteDiff(Bi, Fo_m, dFo, knots)
        do q = 1,3
            open(q, file='TempX/TempX'//trim(str(alpha))//'_'//trim(str(q))//'.txt')
            w = 0.1+0.4*(q-1)
            s = string(w*Fo_m, knots)
            write(q,*) thetaMatrix(s,:)*(t_int - t_ext) + t_ext
            write(q,*) (dcoord*(p-1), p=1, knots)
            close(q)
        end do
        if (j==1 .or. j==3) then
            do v=1,3
                open(j, file='TempTime/TempTime'//trim(str(alpha))//'_'//trim(str(v))//'.txt')
                k = column(x_split(v), knots, delta)
                do i=1,n
                    time = (Fo*delta**2)/(4*a)
                    write(j,*) time, thetaMatrix(i,k)*(t_int - t_ext) + t_ext
                    Fo = Fo + dFo
                end do
                Fo = 0
                close(j)
            end do
        end if
        deallocate(thetaMatrix)
    end do
    call cpu_time(finish)
    write(*,'(/,f5.2,a32,/)') finish, 'sec is required for writing data'
    
    !Checking stability
    Bi = (alphaArr(3)*delta)/(2*lmbd)
    Fo_m = a/(delta**2)
    knots = 81
    dX = 1.0/(knots-1)
    dFo = 1.1*(dX**2)/2
    n = Fo_m/dFo + 1
    Fo = 0
    allocate(thetaMatrix(n,knots))
    thetaMatrix = finiteDiff(Bi, Fo_m, dFo, knots)
    do v=1,3
        open(1, file='TempTime/TempTime'//trim(str(v))//'.txt')
        k = column(x_split(v), knots, delta)
        do i=1,n
            time = (Fo*delta**2)/(4*a)
            write(1,*) time, thetaMatrix(i,k)*(t_int - t_ext) + t_ext
            Fo = Fo + dFo
        end do
        Fo = 0
        close(1)
    end do
    deallocate(thetaMatrix)

end program