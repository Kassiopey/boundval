module FiniteM
    contains
    

    character(len=20) function str(k)
        implicit none
		integer :: k
		write (str, *) k
		str = adjustl(str)
    end function


    function column(x, im, delta) result(k)
        implicit none
        real(8) :: x, delta
        integer :: k, im
        k = (2*x/delta)*(im-1) + 1
    end function
    

    function FoMax(Bi)
        implicit none
        real(8) :: FoMax, Bi
        if (Bi < 1.25) then
            FoMax = 3.11/Bi**0.88
        else if (Bi <= 20 .and. Bi >= 1.25) then
            FoMax = 2.76/Bi**0.31
        else
            FoMax = 1.10
        end if
    end function


    function tetaFinite(Bi, Fo, im) result(teta1)
        real(8) :: dX, dFo, Bi, Fo
        integer :: n, im, j
        real(8), dimension(im) :: teta1
        dX = 1.0/(im-1)
        dFo = (dX**2)/2
        n = Fo/dFo + 1 
        teta1(:) = 1
        do j=1,n-1
            teta1 = finiteDiff(teta1, Bi, dFo, im)
        end do
    end function tetaFinite


    function finiteDiff(teta1, Bi, dFo, im)
        implicit none
        real(8) :: dX, dFo, Bi, r
        integer :: i, im
        real(8), dimension(im) :: teta1, finiteDiff
        dX = 1.0/(im-1)
        r = dFo/dX**2
        do i=2,im-1
            finiteDiff(i)=r*(teta1(i+1)+teta1(i-1)) + teta1(i)*(1-2*r)
        end do
        finiteDiff(1) = finiteDiff(2)
        finiteDiff(im) = finiteDiff(im-1)/(1+Bi*dX)
    end function


    function teta(Bi,Fo,eps,X)
        implicit none
        real(8) :: eps, Bi, theta_n, teta, Fo, eps0, mu_n, A_n, X
        integer :: n
        eps0=1e-6
        teta=0
        n=0
        do
            n=n+1
            mu_n = bisection(eps0, n, Bi)
            A_n = 2*sin(mu_n)/(mu_n+sin(mu_n)*cos(mu_n))
            theta_n = A_n*exp(-Fo*mu_n**2)*cos(mu_n*X)
            teta = teta + theta_n
            if (abs(theta_n)<eps) exit  
        end do
    end function


    function bisection(eps,n,Bi) result(c)
        implicit none
        integer :: n
        real(8) :: a,b,fb,c,eps,fc,delta,Bi
        a = acos(-1.0)*(n-1)
        b = acos(-1.0)*(n-0.5)
        fb = 1/tan(b) - b/Bi
        do
            delta=abs(a-b)
            c = (a+b)/2
            fc = 1/tan(c) - c/Bi
            if(fc*fb>0) then
                b=c
                fb = fc
            else
                a=c
            endif 
            if (delta<=eps) exit 
        end do
    end function


end module


program Calc
    use FiniteM
    implicit none        
    real(8) :: Bi, delta=8e-3, lmbd, c, density, t_ext, t_int, a, dX, error, Fo, FoX, FoG ,&
    dFo, time, X, w, x_split(3), coord=0, dcoord, teta_res, eps=1e-3, tetaT, start, finish
    integer :: alphaArr(3), knots(4), n, im, alpha, & 
    i, j, k, s, v, q, p
    real(8), allocatable :: tetaFin(:)
    x_split = [real(8) :: 0, delta/4, delta/2]
    alphaArr = [5, 200, 15000]
    knots = [11, 21, 41, 81]
    t_int = 80      
    t_ext=15
    lmbd=0.74
    density=2500
    c=670
    a=lmbd/(density*c)
    im = 101
    Bi = 0.2
    Fo = 7.42
    X = 0
    allocate(tetaFin(im))
    write(*,'(/,a11,i3,a6)') 'Testing on ', im, ' knots'
    tetaFin = tetaFinite(Bi, Fo, im)
    teta_res = teta(Bi,Fo,eps,X)
    write(*,'(/,a19,f7.5,/a18,f7.5,2/)') 'FiniteAlg result - ', tetaFin(1), 'FurieAlg result - ',teta_res
    deallocate(tetaFin)
    do i=1,4
        im = knots(i)
        allocate(tetaFin(im))
        tetaFin = tetaFinite(Bi, Fo, im)
        error = abs(tetaFin(1)-teta_res)
        write(*,'(a5, i2)') 'Im = ', im
        write(*,'(/,a19,f7.5,/a18,f7.5)') 'FiniteAlg result - ', tetaFin(1), 'FurieAlg result - ',teta_res
        write(*,'(a8,f7.5)') 'Error = ', error
        write(*,'(a17, f5.2, a1, /)') 'Relative error = ', 100*error/teta_res, '%'
        deallocate(tetaFin)
    end do

    im = 81
    dcoord = delta/(2*(im-1))
    dX = 1.0/(im-1)
    dFo = dX**2/2
    allocate(tetaFin(im))
    call cpu_time(start)
    do i=1,3
        alpha = alphaArr(i)
        Bi = (alpha*delta)/(2*lmbd)
        Fo = FoMax(Bi)
        do q = 1,3
            open(q, file='TempX/TempX'//trim(str(alpha))//'_'//trim(str(q))//'.txt')
            w = 0.1+0.4*(q-1)
            FoX = Fo*w
            tetaFin = tetaFinite(Bi, FoX, im)
            write(q,*) tetaFin*(t_int - t_ext) + t_ext
            write(q,*) (dcoord*(p-1), p=1, im)
            close(q)
        end do
        if (i==1 .or. i==3) then
            n = Fo/dFo + 1
            FoG = 0
            do v=1,3
                open(v, file='TempTime/TempTime'//trim(str(alpha))//'_'//trim(str(v))//'.txt')
                k = column(x_split(v), im, delta)
                tetaFin(:) = 1
                do j=1,n-1
                    time = (FoG*delta**2)/(4*a)
                    write(v,*) time, tetaFin(k)*(t_int - t_ext) + t_ext
                    tetaFin = finiteDiff(tetaFin, Bi, dFo, im)
                    FoG = FoG + dFo
                end do
                FoG = 0
                close(v)
            end do
        end if
    end do
    deallocate(tetaFin)
    call cpu_time(finish)
    write(*,'(/,f5.2,a32,/)') finish, 'sec is required for writing data'

    Bi = (alphaArr(3)*delta)/(2*lmbd)
    FoG = a/(delta**2)
    ! im = 81
    ! dX = 1.0/(im-1)
    dFo = 1.1*(dX**2)/2
    n = FoG/dFo + 1
    Fo = 0
    allocate(tetaFin(im))
    do v=1,3
        open(1, file='TempTime/TempTime'//trim(str(v))//'.txt')
        k = column(x_split(v), im, delta)
        tetaFin(:) = 1
        do j=1,n-1
            time = (Fo*delta**2)/(4*a)
            write(1,*) time, tetaFin(k)*(t_int - t_ext) + t_ext
            tetaFin = finiteDiff(tetaFin, Bi, dFo, im)
            Fo = Fo + dFo
        end do
        Fo = 0
        close(1)
    end do
    deallocate(tetaFin)
end program