alpha = [5, 15000];
bi = [0.0027, 81.08];
for i=1:2
    filename = "TempTime/TempTime"+string(alpha(i));
    c = load(filename+"_1.txt");
    s = load(filename+"_2.txt");
    p = load(filename+"_3.txt");
    a = figure;
    x = c(:,1);
    y1 = c(:,2);
    y2 = s(:,2);
    y3 = p(:,2);
    plot(x,y1,'Color', "#3eb489", 'LineWidth',2);
    hold on;
    plot(x,y2,'Color', "#7B68EE", 'LineWidth',2)
    plot(x,y3,'Color', "#F08080", 'LineWidth',2);
    hold off;
    grid on;
    xlabel('t, c', 'fontsize', 11);
    ylabel('T, ℃', 'fontsize', 11);
    title('Bi = '+ string(bi(i)))
    legend('x = 0','x = δ/4','x=δ/2', 'fontsize',10);
    png_name='Graphics/TempTime_δ_bi-'+ string(bi(i))+'.png';
    saveas(a, png_name);
    close
end