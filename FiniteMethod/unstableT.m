q = load('TempTime/TempTime1.txt');
n = load('TempTime/TempTime2.txt');
m = load('TempTime/TempTime3.txt');

a = figure;
x = n(:,1);

y1 = q(:,2);
y2 = n(:,2);
y3 = m(:,2);

plot(x,y1,'Color', "#3eb489", 'LineWidth',2);
hold on;
plot(x,y2,'Color', "#7B68EE", 'LineWidth',2)
plot(x,y3,'Color', "#F08080", 'LineWidth',2);
hold off;
grid on
xlabel('t, с', 'fontsize', 11);
ylabel('T, ℃', 'fontsize', 11);
title('Bi = 81.08')
legend('x = 0','x = δ/4','x=δ/2', 'fontsize',10);
png_name='Graphics/UnstableTemp.png';
saveas(a, png_name);
close;