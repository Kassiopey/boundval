alpha = [5,200,15000];
bi = [0.0027, 1.08, 81.08];
w = load('TempX/TempX5_1.txt');
x = w(2,:);
for i=1:3
    filename = 'TempX/TempX'+string(alpha(i));
    c = load(filename+'_1.txt');
    s = load(filename+'_2.txt');
    p = load(filename+'_3.txt');
    a = figure;
    y1 = c(1,:);
    y2 = s(1,:);
    y3 = p(1,:);
    plot(x,y1,'Color', "#3eb489", 'LineWidth',2);
    hold on;
    plot(x,y2,'Color', "#7B68EE", 'LineWidth',2)
    plot(x,y3,'Color', "#F08080", 'LineWidth',2);
    hold off;
    grid on;
    xlabel('x, м', 'fontsize', 11);
    ylabel('T, ℃', 'fontsize', 11);
    title('Bi = '+ string(bi(i)))
    legend('Fo = 0.1FoMax','Fo = 0.5FoMax','Fo = 0.9FoMax', 'fontsize',10);
    png_name='Graphics/TempX_bi-'+ string(bi(i))+'.png';
    saveas(a, png_name);
    close
end